﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Clase de la que heredan PlayerControl y EnemyControl.
/// </summary>
public abstract class CharacterControl : MonoBehaviour {

    protected PlatformerMotor2D motor;
    protected ShootControl shootCtrl;
    protected StateControl stateCtrl;
    protected DamageControl damageCtrl;
    protected AnimatorControl animatorCtrl;

    [SerializeField]
    private bool canWalk;
    [SerializeField]
    private bool canJump;
    [SerializeField]
    private bool canShoot;
    [SerializeField]
    private bool canBeInvincible;

    public bool CanWalk {
        get { return canWalk; }
    }

    public bool CanJump {
        get { return canJump; }
    }

    public bool CanShoot {
        get { return canShoot; }
    }

    public bool CanBeInvincible   {
        get { return canBeInvincible; }
    }

    protected void Start() {
        motor = GetComponent<PlatformerMotor2D>();
        shootCtrl = GetComponent<ShootControl>();
        stateCtrl = GetComponent<StateControl>();
        damageCtrl = GetComponent<DamageControl>();
        animatorCtrl = GetComponent<AnimatorControl>();
    }

    protected void Shoot() {
    }

    public abstract void Die();

    public abstract void OnLifeChanged(int currentLife, int lifeChange, GameObject source);

}
