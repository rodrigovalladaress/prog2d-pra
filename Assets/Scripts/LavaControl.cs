﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Se encarga de dañar a los personajes que tocan la lava.
/// </summary>
public class LavaControl : MonoBehaviour {

    private const int LAVA_DAMAGE = 6;

    public void OnTriggerStay2D(Collider2D other) {
        Harm(other.gameObject);
    }

    public void OnTriggerEnter2D(Collider2D other) {
        Harm(other.gameObject);
    }

    private void Harm(GameObject other) {
        other.GetComponent<DamageControl>().Harm(LAVA_DAMAGE);
    }

}
