﻿using UnityEngine;
using System.Collections;
/// <summary>
/// StateControl guarda información sobre el estado de los personajes (si están caminando, 
/// saltando, disparando, etc.). Es útil para gestionar las animaciones y el estado de la
/// partida.
/// </summary>
public class StateControl : MonoBehaviour {

    public enum CharacterState {
        onAir,
        idle,
        walking,
        shooting,
        dead
    }

    public bool facingLeft;


    private CharacterState state;
    private PlatformerMotor2D motor;

    [SerializeField]
    private bool shooting;
    private bool canShootNow;
    private bool invincible;
    [SerializeField]
    private bool dead;

    public CharacterState State {
        get { return state; }
    }

    public bool Shooting {
        get { return shooting; }
        set { shooting = value; }
    }

    public bool CanShootNow {
        get { return canShootNow; }
        set { canShootNow = value; }
    }

    public float DirectionFacing {
        get {
            return motor ? (motor.facingLeft ? Directions.LEFT : Directions.RIGHT) : 0f;
        }
    }

    public bool Invincible {
        get { return invincible; }
        set { invincible = value; }
    }

    public bool Dead {
        get { return dead; }
        set { dead = value; }
    }

    void Start() {
        motor = GetComponent<PlatformerMotor2D>();
        shooting = false;
        canShootNow = true;
    }

    void Update() {
        facingLeft = motor.facingLeft;
        if (!Dead) {
            if (shooting) {
                state = CharacterState.shooting;
            }
            else {
                if (motor.motorState == PlatformerMotor2D.MotorState.Falling ||
                motor.motorState == PlatformerMotor2D.MotorState.FallingFast ||
                motor.motorState == PlatformerMotor2D.MotorState.Jumping) {
                    state = CharacterState.onAir;
                }
                else if (Mathf.Abs(motor.velocity.x) > 0f) {
                    state = CharacterState.walking;
                }
                else {
                    state = CharacterState.idle;
                }
            }
        }
        else {
            state = CharacterState.dead;
        }
    }
}
