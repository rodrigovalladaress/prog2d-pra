﻿using UnityEngine;
using System.Collections;
/// <summary>
/// AnimatorControl se encarga de la gestión de las animaciones de los personajes.
/// </summary>
public class AnimatorControl : MonoBehaviour {

    private const float SECS_OF_INVISIBILITY = 0.1f;
    private const float SECS_FOR_NEXT_BLINK = 0.2f;

    [SerializeField]
    private GameObject visualChild;

    private CharacterControl characterCtrl;
    private StateControl stateCtrl;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    private float lastDirectionFacing;
    private float directionFacing;

    private bool jumpingParam;
    private bool walkingParam;
    private bool shootingParam;
    [SerializeField]
    private bool deadParam;

    private bool blinking;

    // Use this for initialization
    void Start () {
        if (!visualChild) {
            Debug.LogError("visualchild of " + name + " is not assigned.");
        }
        characterCtrl = GetComponent<CharacterControl>();
        stateCtrl = GetComponent<StateControl>();
        animator = visualChild.GetComponent<Animator>();
        spriteRenderer = visualChild.GetComponent<SpriteRenderer>();
        lastDirectionFacing = stateCtrl.DirectionFacing;
        directionFacing = lastDirectionFacing;
        blinking = false;
    }
	
	void Update () {
        directionFacing = stateCtrl.DirectionFacing;
        if (directionFacing != lastDirectionFacing) {
            Flip();
        }
        lastDirectionFacing = directionFacing;
        switch (stateCtrl.State) {
            case StateControl.CharacterState.idle:
                walkingParam = false;
                jumpingParam = false;
                shootingParam = false;
                deadParam = false;
                break;
            case StateControl.CharacterState.walking:
                walkingParam = true;
                jumpingParam = false;
                shootingParam = false;
                deadParam = false;
                break;
            case StateControl.CharacterState.onAir:
                jumpingParam = true;
                shootingParam = false;
                deadParam = false;
                break;
            case StateControl.CharacterState.shooting:
                shootingParam = true;
                deadParam = false;
                break;
            case StateControl.CharacterState.dead:
                deadParam = true;
                break;
        }
        UpdateAnimatorParams();
    }

    private void UpdateAnimatorParams() {
        if(characterCtrl.CanWalk)
            animator.SetBool(AnimatorParamNames.WALKING, walkingParam);
        if(characterCtrl.CanJump)
            animator.SetBool(AnimatorParamNames.JUMPING, jumpingParam);
        if (characterCtrl.CanShoot) {
            animator.SetBool(AnimatorParamNames.SHOOTING, shootingParam);
        }
        if (characterCtrl.CanBeInvincible && !blinking && stateCtrl.Invincible) {
            StartCoroutine("Blink");
            blinking = true;
        }
        if (tag == Tag.PLAYER) {
            animator.SetBool(AnimatorParamNames.DEAD, deadParam);
        }
    }

    private IEnumerator Blink() {
        if (stateCtrl.Invincible) {
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(SECS_OF_INVISIBILITY);
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(SECS_FOR_NEXT_BLINK);
            StartCoroutine("Blink");
        }
        else {
            StopCoroutine("Blink");
            blinking = false;
        }
    }

    public void Flip() {
        Vector3 newLocalScale = visualChild.transform.localScale;
        newLocalScale.x *= -1;
        visualChild.transform.localScale = newLocalScale;
    }
}
