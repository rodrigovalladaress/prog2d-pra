﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// Movimiento vertical del texto de créditos.
/// </summary>
public class CreditsControl : MonoBehaviour {

    [SerializeField]
    private float speed;
    [SerializeField]
    private GameObject credits;
    [SerializeField]
    private float staffRollDelay;
    [SerializeField]
    private float maxY;

    private bool staffRoll;

	void Start () {
        staffRoll = false;
        StartCoroutine("StaffRollCoroutine");
	}

	void Update () {
        if (staffRoll && credits.transform.position.y < maxY) {
            credits.transform.Translate(Vector2.up * Time.deltaTime * speed);
            Debug.Log(credits.transform.position.y);
        }
	}

    private IEnumerator StaffRollCoroutine() {
        yield return new WaitForSeconds(staffRollDelay);
        staffRoll = true;
    }

}
