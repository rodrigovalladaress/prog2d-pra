﻿using UnityEngine;
using System.Collections;
/// <summary>
/// TriangleRockControl se encarga de mover la roca triangular cuando se obtienen todos los
/// diamantes de una sección.
/// </summary>
public class TriangleRockControl : MonoBehaviour {

    private DiamondDoorControl diamondDoorControl;

    private Vector2 startPosition;
    private float startTime;
    private float distToParent;

    private bool allDiamonsAcquired;

    public bool AllDiamondsAcquired
    {
        get { return allDiamonsAcquired; }
        set { allDiamonsAcquired = value; }
    }

    void Start () {
        diamondDoorControl = transform.parent.parent.GetComponent<DiamondDoorControl>();
        startPosition = transform.position;
        distToParent = Vector2.Distance(transform.parent.position, startPosition);
        startTime = -1f;
    }
	
	void Update () {
        if (allDiamonsAcquired) {
            if (transform.position != transform.parent.position) {
                if (startTime == -1f) {
                    startTime = Time.time;
                }
                float distCovered = (Time.time - startTime) * diamondDoorControl.TriangleRocksSpeed;
                float fracJourney = distCovered / distToParent;
                transform.position = Vector3.Lerp(startPosition, transform.parent.position, 
                    fracJourney);
            }
            else {
                enabled = false;
            }
        }
	}
}
