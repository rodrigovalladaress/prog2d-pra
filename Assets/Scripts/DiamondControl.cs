﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Comportamiento de los diamantes cuando se obtienen.
/// </summary>
public class DiamondControl : MonoBehaviour {

    private bool acquired;

    private DiamondDoorControl diamondDoorControl;
    private DiamondHollowControl diamondHollowCtrl;
    private BoxCollider2D boxCollider;

    private Vector2 startPosition;
    private float startTime;
    private float distToParent;

    public void DiamondGot() {
        acquired = true;
        transform.Translate((transform.parent.position - transform.position) * Time.deltaTime);
    }

    void Start () {
        diamondDoorControl = transform.parent.parent.GetComponent<DiamondDoorControl>();
        diamondHollowCtrl = transform.parent.GetComponent<DiamondHollowControl>();
        boxCollider = GetComponent<BoxCollider2D>();
        startPosition = transform.position;
        distToParent = Vector2.Distance(transform.parent.position, startPosition);
        startTime = -1f;
    }

	void Update () {
        if (acquired) {
            if (transform.position != transform.parent.position) {
                if (boxCollider.enabled) {
                    boxCollider.enabled = false;
                }
                if (startTime == -1f) {
                    startTime = Time.time;
                }
                float distCovered = (Time.time - startTime) * diamondDoorControl.DiamondsSpeed;
                float fracJourney = distCovered / distToParent;
                transform.position = Vector3.Lerp(startPosition, transform.parent.position, fracJourney);
            }
            else {
                diamondHollowCtrl.DiamondAcquired();
                DestroyDiamond();
            }
        }
    }

    void DestroyDiamond() {
        GameObject.Destroy(gameObject);
    }

}
