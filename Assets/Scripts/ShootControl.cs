﻿using UnityEngine;
using System.Collections;
/// <summary>
/// ShootControl se encarga de la gestión de los disparos de los personajes.
/// </summary>
public class ShootControl : MonoBehaviour {

    private StateControl stateCtrl;

    [SerializeField]
    private float shootTime;
    [SerializeField]
    private float shootDelay;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private GameObject shootPoint;
    [SerializeField]
    private AudioClip shootSound;

    private AudioSource audioSrc;

	void Start () {
        stateCtrl = GetComponent<StateControl>();
        audioSrc = GetComponent<AudioSource>();
	}

    public void Shoot() {
        StartCoroutine("ShootCouroutine");
    }

    private IEnumerator ShootCouroutine() {
        GameObject b;
        float direction = stateCtrl.DirectionFacing;
        stateCtrl.CanShootNow = false;
        stateCtrl.Shooting = true;
        yield return new WaitForSeconds(shootDelay);
        b = (GameObject)Instantiate(bulletPrefab, shootPoint.transform.position,
            Quaternion.identity);
        b.transform.SetParent(Bullet.bulletsParent);
        if (direction == Directions.LEFT) {
            b.GetComponent<SpriteRenderer>().flipX = true;
        }
        b.GetComponent<PlatformerMotor2D>().normalizedXMovement = direction;
        if(shootSound) {
            audioSrc.clip = shootSound;
            audioSrc.Play();
        }
        yield return new WaitForSeconds(shootTime);
        stateCtrl.CanShootNow = true;
        stateCtrl.Shooting = false;
        StopCoroutine("ShootCouroutine");
    }

    public float ShootPointPosition() {
        return shootPoint.transform.position.x < transform.position.x ? Directions.LEFT : 
            Directions.RIGHT;
    }

}
