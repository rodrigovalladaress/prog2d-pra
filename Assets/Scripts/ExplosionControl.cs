﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Se encarga de la gestión de las animaciones de explosión.
/// </summary>
public class ExplosionControl : MonoBehaviour {

    [SerializeField]
    private GameObject explosionPrefab;

    private Animator animator;
    private AudioSource audioSrc;

	void Start () {
        animator = GetComponent<Animator>();
        animator.SetBool("Explode", true);
        audioSrc = GetComponent<AudioSource>();
        audioSrc.Play();
    }

}