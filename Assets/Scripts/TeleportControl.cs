﻿using UnityEngine;
using System.Collections;
/// <summary>
/// TeleportControl se encarga de teletransportar al jugador del nivel 1 al nivel 2.
/// </summary>
public class TeleportControl : MonoBehaviour {

    [SerializeField]
    private string toSceneName;

    GameManager gameMngr;
    AudioSource audioSrc;

	void Start () {
        gameMngr = GameObject.Find("GameManager").GetComponent<GameManager>();
        audioSrc = GetComponent<AudioSource>();
    }

    public void OnTriggerEnter2D(Collider2D other) {
        audioSrc.Play();
        gameMngr.LoadScene(toSceneName);
    }

}
