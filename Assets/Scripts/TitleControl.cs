﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// TitleControl se encarga de mostrar y ocultar la información del juego en la pantalla del título
/// (historia, objetivo del juego y controles).
/// </summary>
public class TitleControl : MonoBehaviour {

    private const string infoString = "[i]";

    private const string closeString = "[volver]";

    [SerializeField]
    private GameObject toggleButtonTextGameObject;
    [SerializeField]
    private GameObject[] titleGameObjects;
    [SerializeField]
    private GameObject[] infoGameObjects;

    private Text toggleButtonText;

    private bool infoScreen;

	void Start () {
        infoScreen = false;
        SetActiveInArray(infoGameObjects, false);
        toggleButtonText = toggleButtonTextGameObject.GetComponent<Text>();
	}

    public void Toggle() {
        if (!infoScreen) {
            SetActiveInArray(titleGameObjects, false);
            SetActiveInArray(infoGameObjects, true);
            toggleButtonText.text = closeString;
        }
        else {
            SetActiveInArray(titleGameObjects, true);
            SetActiveInArray(infoGameObjects, false);
            toggleButtonText.text = infoString;
        }
        infoScreen = !infoScreen;
    }

    private void SetActiveInArray(GameObject[] array, bool active) {
        foreach (GameObject item in array) {
            item.SetActive(active);
        }
    }

}
