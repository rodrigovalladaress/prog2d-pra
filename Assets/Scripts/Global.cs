﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Constantes utilizadas en varias clases.
/// </summary>
public static class InputKeyNames {
    public const string HORIZONTAL = "Horizontal";
    public const string VERTICAL = "Vertical";
    public const string JUMP = "Jump";
    public const string FIRE = "Fire1";
}

public static class AnimatorParamNames {
    public const string WALKING = "Walking";
    public const string JUMPING = "Jumping";
    public const string SHOOTING = "Shooting";
    public const string BLINK = "Blink";
    public const string DEAD = "Dead";
}

public static class Directions {
    public const float LEFT = -1f;
    public const float RIGHT = 1f;
    public enum DirEnum {
        left,
        right
    }
    public static float GetDirectionFloat(DirEnum dir) {
        float r = 0f;
        switch (dir) {
            case DirEnum.left:
                r = LEFT;
                break;
            case DirEnum.right:
                r = RIGHT;
                break;
        }
        return r;
    }
}

public static class Tag {
    public const string BULLET = "bullet";
    public const string PLAYER = "player";
    public const string WALL = "wall";
    public const string DIAMOND = "diamond";
}

public static class Layers {
    public const int CHARACTER = 11;
    public const int GROUND = 10;
    public const int ENEMY = 14;
}
