﻿using UnityEngine;
using System.Collections;
/// <summary>
/// SavePointControl se encarga de la gestión de los puntos de guardado (pilares de regeneración).
/// </summary>
public class SavePointControl : MonoBehaviour {

    private GameManager gameMngr;

    void Start () {
        gameMngr = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == Tag.PLAYER) {
            gameMngr.CurrentSpawnPoint = gameObject;
            other.gameObject.GetComponent<PlayerControl>().RestoreLife();
        }
    }

}
