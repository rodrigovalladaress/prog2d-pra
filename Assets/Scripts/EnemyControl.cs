﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// Sse encarga de la gestión del comportamiento de los enemigos.
/// </summary>
public class EnemyControl : CharacterControl {

    private const float SECONDS_CHANGING_DIR = 1f;

    [SerializeField]
    private bool facePlayer;
    [SerializeField]
    private Directions.DirEnum startDirection;
    [SerializeField]
    private float timeBetweenShoots;

    private float direction;
    private bool canChangeDirection;

    private new void Start() {
        base.Start();
        direction = Directions.GetDirectionFloat(startDirection);
        canChangeDirection = true;
        // Si el personaje no puede caminar y va a mirar hacia la izquierda, hay que cambiar
        // facingLeft manualmente en Platformer Motor 2D.
        if (direction == Directions.LEFT && !CanWalk) {
            motor.facingLeft = true;
        }
    }

    void Update() {
        if (facePlayer) {
            FlipToFacePlayer();
        }
        if (CanWalk) {
            motor.normalizedXMovement = direction;
        }
        if (CanShoot && stateCtrl.CanShootNow) {
            shootCtrl.Shoot();
            stateCtrl.Shooting = true;
        }
    }

    private void FlipToFacePlayer() {
        bool playerOnLeft = GameObject.FindGameObjectWithTag(Tag.PLAYER).transform.position.x < 
            transform.position.x;
        if ((playerOnLeft && stateCtrl.DirectionFacing == Directions.RIGHT) || 
            (!playerOnLeft && stateCtrl.DirectionFacing == Directions.LEFT)) {
            if (!CanWalk) {
                motor.facingLeft = !motor.facingLeft;
            }
            else {
                ChangeDirection();
            }
        }
    }

    public void OnTriggerStay2D(Collider2D other) {
        ManageCollisionForHarm(other.gameObject);
        if (other.tag.ToLower() == Tag.WALL.ToLower() && !facePlayer) {
            ChangeDirection();
        }
    }

    public void OnTriggerEnter2D(Collider2D other) {
        ManageCollisionForHarm(other.gameObject);
        if (other.tag.ToLower() == Tag.WALL.ToLower() && !facePlayer) {
            ChangeDirection();
        }
    }

    private void ChangeDirection() {
        if (canChangeDirection) {
            direction *= -1;
            StartCoroutine("ChangeDirecionCoroutine");
        }
    }    

    private void ManageCollisionForHarm(GameObject other) {
        if (other.gameObject.tag == Tag.PLAYER) {
            other.GetComponent<DamageControl>().Harm(damageCtrl.Power);
        }
    }

    private IEnumerator ChangeDirecionCoroutine() {
        canChangeDirection = false;
        yield return new WaitForSeconds(SECONDS_CHANGING_DIR);
        canChangeDirection = true;
        StopCoroutine("ChangeDirecionCoroutine");
    }

    public override void Die() {
        GameObject.Destroy(gameObject);
    }

    public override void OnLifeChanged(int currentLife, int lifeChange, GameObject source) {

    }
}
