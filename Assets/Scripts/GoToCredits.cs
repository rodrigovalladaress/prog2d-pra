﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Teletransporta al jugador a la pantalla de créditos (desde el nivel 2).
/// </summary>
public class GoToCredits : MonoBehaviour {

    GameManager gameMngr;
    AudioSource audioSrc;

    void Start() {
        GameObject musicManager = GameObject.Find("MusicManager");
        gameMngr = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (musicManager) {
            audioSrc = musicManager.GetComponent<AudioSource>();
        }
    }

    public void OnTriggerEnter2D(Collider2D other) {
        gameMngr.LoadScene("Credits");
        if (audioSrc) {
            audioSrc.Stop();
        }
    }

}
