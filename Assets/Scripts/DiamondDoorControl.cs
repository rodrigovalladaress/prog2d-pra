﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Mecanismo de puerta que separa las distintas secciones de la cueva.
/// </summary>
public class DiamondDoorControl : MonoBehaviour {

    [SerializeField]
    GameObject[] hollows;
    [SerializeField]
    GameObject[] triangleRocks;

    [SerializeField]
    private float diamondsSpeed;
    [SerializeField]
    private float triangleRockSpeed;

    [SerializeField]
    private Sprite diamondAcquiredSprite;

    private int diamondsAcquired;

    public Sprite DiamondAcquiredSprite
    {
        get { return diamondAcquiredSprite; }
    }

    public float DiamondsSpeed
    {
        get { return diamondsSpeed; }
    }

    public float TriangleRocksSpeed
    {
        get { return triangleRockSpeed; }
    }

    void Start() {
        diamondsAcquired = 0;
    }

    public void DiamondAcquired() {
        diamondsAcquired++;
        if (diamondsAcquired == hollows.Length) {
            foreach (GameObject triangleRock in triangleRocks) {
                triangleRock.GetComponent<TriangleRockControl>().AllDiamondsAcquired = true;
            }
        }
    }
}
